public interface Lecteur 
{
    public boolean ouvrir();
    public void lire();
    public void affiche();
}
