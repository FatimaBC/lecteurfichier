# README #
# Mini projet : Lecteur de fichier #
## Consignes ##
1. Définir une interface de lecteur de fichiers
2. Plusieurs sous-classes pour différents types de fichiers
3. Utiliser une classe abstraite principale pour définir les méthodes qui ne changeront pas d'un fichier à l'autre
4. Implémenter une des classes qui affiche le fichier à l'envers à l'écran (en terme de lignes)
(Bonus)
5. Implémenter une des casses qui affiche le fichier de manière palindromique (en terme de caractères)
6. Comparateur de fichier type <diff>
## Pour vous aider ##
**Classes:
0. LecteurFichier
a. ->ouvrir(), File
b. ->lire(), FileInputStream
c. ->affiche(), syso
0. LecteurFichierReverse (extends LecteurFichier)
a. ->affiche()