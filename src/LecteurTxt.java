import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class LecteurTxt extends LecteurTout implements Lecteur, LecteurReverse
{
    private FileInputStream flux;
    public LecteurTxt(String unChemin)
    {
        super(unChemin);
    }
    public void lire()
    {
	try 
        {
            //instance de FileInputStream associé au fichier
            this.flux = new FileInputStream(this.getFile()); 
            //instance qui effectue la conversion octets.caractère
            InputStreamReader isr=new InputStreamReader(this.flux);
            //instance qui permet de lire ligne par ligne
            BufferedReader br=new BufferedReader(isr);          
            String ligne;
            try 
            {
                while ((ligne=br.readLine())!=null) 
                    this.getContenu().add(ligne);

                br.close();
            } 
            catch (IOException e) 
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        catch (FileNotFoundException ex) 
        {
           System.err.println(ex);
        }    	        
    }
    public void affiche()
    {
    	if(this.getContenu().isEmpty())
    	{
    		this.getContenu().add("Fichier Vide");
    	}
    	for(String ligne:this.getContenu())
    	{
    		System.out.println(ligne);
    	}       
    }
   public void afficheReverse()
    {
        if(this.getContenu().isEmpty())
        {
            this.getContenu().add("Fichier Vide");
        }
        for (int i=this.getContenu().size()-1; i>=0; i--)
        {
            System.out.println(this.getContenu().get(i));
        }    	
    }
   public void palyndromique()
   {
        for(String ligne:this.getContenu())
    	{
            char[] laligne=ligne.toCharArray();
            for (char l:laligne)
            {
                System.out.println(l);
            } 
    	}  
       for(String ligne:this.getContenu())
    	{
            char[] laligne=ligne.toCharArray();
            for (int i=laligne.length-1; i>=0; i--)
            {
                System.out.println(laligne[i]);
            }   
    	}
       
   }
}
