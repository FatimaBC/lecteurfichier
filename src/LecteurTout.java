import java.io.File;
import java.util.ArrayList;
public abstract class LecteurTout 
{ 
    private String cheminFile;
    private ArrayList<String> contenu;
    private File fichier;
    public LecteurTout(String unChemin)
    {
        this.cheminFile=unChemin;
        this.fichier = new File (this.cheminFile);
        this.contenu=new ArrayList<String>();
    }
    public boolean ouvrir()
    {
        boolean bool=false;
        if(this.fichier.exists())
        { 
            bool=true;
        }
        else
        {
            System.err.println("Le fichier n'existe pas.");
        }
        return bool;
    }  
    abstract void lire();
    public void affiche()
    {
    	if(this.contenu.isEmpty())
    	{
    		this.contenu.add("Fichier Vide");
    	}
    	for(String ligne:contenu)
    	{
    		System.out.println(ligne);
    	}       
    }
    //getters
    public String getCheminFile()
    {
        return cheminFile;
    }
    public ArrayList<String> getContenu()
    {
        return contenu;
    }
    public File getFile()
    {
        return this.fichier;
    }
}